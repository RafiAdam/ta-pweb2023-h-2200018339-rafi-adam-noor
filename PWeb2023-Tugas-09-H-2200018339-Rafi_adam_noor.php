<?php
echo "=== Program 9.1: Menghitung Gaji ===<br><br>";

$gaji = 1000000;
$pajak = 0.1;
$thp = $gaji - ($gaji * $pajak);

echo "Gaji sebelum pajak: Rp " . number_format($gaji) . "<br>";
echo "Gaji bersih yang dibawa pulang: Rp " . number_format($thp);

?>


<?php
echo "<br><br>=== Program 9.2: Operasi Perbandingan ===<br><br>";

$a = 5;
$b = 4;
echo "1 = True<br>";
echo "$a sama dengan $b: " . ($a == $b);
echo "<br>$a tidak sama dengan $b: " . ($a != $b);
echo "<br>$a kurang dari $b: " . ($a < $b);
echo "<br>$a lebih dari $b: " . ($a > $b);
echo "<br>($a tidak sama dengan $b) dan ($a lebih dari $b): " . (($a != $b) && ($a > $b));
echo "<br>($a tidak sama dengan $b) atau ($a lebih dari $b): " . (($a != $b) || ($a > $b))."<br><br>";



?>